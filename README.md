# LayTp极速后台开发框架

#### 介绍
基于ThinkPHP5.1+layui的一个极速后台开发框架。
laytp完全免费，无任何商业授权版本，只是需要保留laytp的标志信息。

#### 官网
http://www.laytp.com

#### 演示
- 地址:http://demo.laytp.com
- 账号:demo
- 密码:123456

#### 默认功能
- RBAC权限控制
  - 管理员管理
  - 角色管理
  - 菜单管理
  - 操作日志
- 常规管理
  - 系统配置
  - 个人设置
  - 地区管理
  - 附件管理
- 插件功能
  - 支持远程和本地插件的安装和卸载
  - 目前有一个自动生成的官方插件，可视为laytp框架的一部分，他的主要功能包括：
    - 可视化一键生成Curd
    - 自动生成菜单
    - 自动生成Api文档
- 常用接口
  - 在application/api/下，框架已经自带了一些常用的api接口，使用自动生成Api文档的功能，可以生成和查看Api文档


#### 使用文档

http://www.laytp.com/document/index/id/2.html

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
